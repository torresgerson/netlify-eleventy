module.exports = function (eleventyConfig){
    
    eleventyConfig.addPassthroughCopy("images");
    const md = require("markdown-it")({
        html: true,
        linkify: false,
        typographer: false
    })

    eleventyConfig.setLibrary("md", md);

}
